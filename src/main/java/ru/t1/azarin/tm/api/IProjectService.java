package ru.t1.azarin.tm.api;

import ru.t1.azarin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    void clear();

    List<Project> findAll();

    Project create(String name);

    Project create(String name, String description);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
